object Form5: TForm5
  Left = 455
  Height = 318
  Top = 45
  Width = 474
  BorderStyle = bsDialog
  Caption = 'Sytem Reboot'
  ClientHeight = 318
  ClientWidth = 474
  DesignTimePPI = 150
  Position = poScreenCenter
  LCLVersion = '7.0'
  object ListBox1: TListBox
    Left = 8
    Height = 216
    Top = 16
    Width = 464
    Font.Style = [fsBold]
    ItemHeight = 0
    ParentFont = False
    ScrollWidth = 462
    TabOrder = 0
    TopIndex = -1
  end
  object Button1: TButton
    Left = 56
    Height = 39
    Top = 272
    Width = 117
    Caption = 'Reboot'
    OnClick = Button1Click
    TabOrder = 1
  end
  object Button2: TButton
    Left = 288
    Height = 39
    Top = 272
    Width = 117
    Caption = 'Cancel'
    OnClick = Button2Click
    TabOrder = 2
  end
  object plURLLabel1: TplURLLabel
    Cursor = crHandPoint
    Left = 72
    Height = 26
    Top = 240
    Width = 324
    Caption = 'Project''s Wiki - V4L2 Limitations'
    Font.Color = clBlue
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
  end
end
