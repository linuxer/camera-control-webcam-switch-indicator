unit About;

{$mode objfpc}{$H+}
{$warnings off}
{$hints off}

// Copyright © 2020 linuxer <linuxer@artixlinux.org>  <https://linuxer.gr>
// Created at 23th of January 2020, by Linuxer (https://gitlab.com/linuxergr), from scratch with Free Pascal
// Redesigned and further Developed at 28th of January 2020, by Initial developer
// to provide Camera and Mic status alone with On/Off and Mute/Unmute fuctions
// Developed further for intrusion feeling and logging at 2nd of February 2020, by Initial developer
// Developed for Blacklisting/Whitelisting functions for both camera & audio at 7th of February 2020, by Initial developer
// Finalized, except traslations at 15th of February 2020.
// Further Capabilities added alone with better Logging at 22nd of February 2020
// Essential Development Ended at 5th of March 2020. Minor Updates will follow, if needed
// Thanks to Ido Kanner idokan at@at gmail dot.dot com, libnotify has been added to the project, so to solve issues of notifiers and Qt5 on Linux
// https://forum.lazarus.freepascal.org/index.php/topic,52052.0.html
// Wayland and x11 operation confirmed too with the libnotify addition
// Further project development for the next major release 2.1.0, as from 3rd of Nov 2020 / ended at 6th of Nov 2020

interface

uses
  {$IFDEF UNIX}
          cthreads,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  LCLIntf;

type

  { TForm2 }

  TForm2 = class(TForm)
    Button2            : TButton;
    Image1             : TImage;
    Image2             : TImage;
    Image              : TImage;
    ImageList          : TImageList;
    Label1             : TLabel;
    Label2             : TLabel;
    Label3             : TLabel;
    ListBox1           : TListBox;
    StaticText1        : TStaticText;
    StaticText2        : TStaticText;
    StaticText3        : TStaticText;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StaticText1Click(Sender: TObject);
    procedure StaticText2Click(Sender: TObject);
    procedure StaticText3Click(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.frm}

{ TForm2 }

procedure TForm2.Button2Click(Sender: TObject);
begin
     Form2.Close;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin

end;

procedure TForm2.StaticText1Click(Sender: TObject);
begin
     OpenUrl('https://gitea.artixlinux.org/linuxer');
end;

procedure TForm2.StaticText2Click(Sender: TObject);
begin
     OpenUrl('https://wiki.freepascal.org/FPC_modified_LGPL');
end;

procedure TForm2.StaticText3Click(Sender: TObject);
begin
     OpenUrl('https://gitea.artixlinux.org/linuxer/camera-control-webcam-switch-indicator');
end;

end.
