unit FilesList;

{$mode objfpc}{$H+}

// Created at 23th of January 2020, by Linuxer (https://gitlab.com/psposito), from scratch with Free Pascal
// Redesigned and further Developed at 28th of January 2020, by Initial developer
// to provide Camera and Mic status alone with On/Off and Mute/Unmute fuctions
// Developed further for intrusion feeling and logging at 2nd of February 2020, by Initial developer
// Developed for Blacklisting/Whitelisting functions for both camera & audio at 7th of February 2020, by Initial developer
// Finalized, except traslations at 15th of February 2020.
// Further Capabilities added alone with better Logging at 22nd of February 2020

interface

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, TplLabelUnit,
  LCLIntf, ExtCtrls, Logs, Unix;

type

  { TForm6 }

  TForm6 = class(TForm)
    Button1     : TButton;
    Image       : TImage;
    ImageList   : TImageList;
    ListBox     : TListBox;
    procedure Button1Click(Sender: TObject);

  private

  public

  end;

var
  Form6           : TForm6;

implementation

{$R *.frm}

{ TForm6 }

procedure TForm6.Button1Click(Sender: TObject);
begin
     Form6.Hide;
end;

end.